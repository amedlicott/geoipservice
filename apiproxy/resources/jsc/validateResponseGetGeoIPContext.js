print ( "entering ValidateGetGeoIPContext");

var schema = 
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "definitions": {
    "GeoIP_1": {
      "required": [
        "ReturnCode"
      ],
      "type": "object",
      "properties": {
        "ReturnCode": {
          "type": "integer",
          "format": "int32"
        },
        "IP": {
          "type": "string"
        },
        "ReturnCodeDetails": {
          "type": "string"
        },
        "CountryName": {
          "type": "string"
        },
        "CountryCode": {
          "type": "string"
        }
      }
    }
  },
  "required": [
    "GeoIP"
  ],
  "type": "object",
  "additionalProperties": false,
  "properties": {
    "GeoIP": {
      "required": [
        "ReturnCode"
      ],
      "allOf": [
        {
          "$ref": "#/definitions/GeoIP"
        },
        {
          "properties": {
            "ReturnCode": {
              "type": "integer",
              "format": "int32"
            },
            "IP": {
              "type": "string"
            },
            "ReturnCodeDetails": {
              "type": "string"
            },
            "CountryName": {
              "type": "string"
            },
            "CountryCode": {
              "type": "string"
            }
          }
        }
      ]
    }
  }
};

var valid = tv4.validate(JSON.parse(response.content), schema);
if(valid){
    print("Schema is valid!");
} else {

    var json = {};
    json.ResponseValidationError = tv4.error;
    context.setVariable("response.content", JSON.stringify(json));
    context.setVariable("response.status.code", 500);

  }