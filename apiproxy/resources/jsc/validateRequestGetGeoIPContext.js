print ( "entering ValidateGetGeoIPContext");

var schema = 
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "additionalProperties": false
};

var valid = tv4.validate(JSON.parse(request.content), schema);
if(valid){
    print("Schema is valid!");
} else {
    context.setVariable("error.status.code", "500");
    context.setVariable("error.reason.phrase", "Request schema validation error");
    context.setVariable("error.content", tv4.error);
    context.setVariable("error.header.Content-Type","application/json");
  }